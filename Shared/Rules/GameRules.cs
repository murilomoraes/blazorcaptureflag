using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorCaptureFlag.Shared.Model;

namespace BlazorCaptureFlag.Shared.Rules
{
    public static class GameRules
    {
        private static Dictionary<GameMove, Action<Player, GameBoard>> AcceptedMoves =
            CreateAcceptedMoves();

        private static Dictionary<GameMove, Action<Player, GameBoard>> CreateAcceptedMoves()
        {
            return new Dictionary<GameMove, Action<Player, GameBoard>>
            {
                { 
                    GameMove.Up,
                    (p, b) => p.Y = Math.Max(p.Y - 1, 0)
                },
                { 
                    GameMove.Down,
                    (p, b) => p.Y = Math.Min(p.Y + 1, b.Height - 1)
                },
                { 
                    GameMove.Left,
                    (p, b) => p.X = Math.Max(p.X - 1, 0)
                },
                { 
                    GameMove.Right,
                    (p, b) => p.X = Math.Min(p.X + 1, b.Width - 1)
                }
            };
        }

        public static bool MovePlayer(Player player, GameBoard board, GameMove move)
        {
            if (player != null &&
                AcceptedMoves.TryGetValue(move, out Action<Player, GameBoard> action))
            {
                action(player, board);
                return true;
            }

            return false;
        }
    }
}