namespace BlazorCaptureFlag.Shared.Model
{
    public class Player
    {
        public string Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Points { get; set; } = 0;

        public void IncrementPoints() => Points += 1;
        public void ResetPoints() => Points = 0;

        public Player()
        {
            
        }

        public override string ToString()
        {
            return $"{{ Id: {Id}, X: {X}, Y: {Y} }}";
        }
    }
}