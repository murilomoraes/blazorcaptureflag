using System.Collections.Generic;
using System.Linq;

namespace BlazorCaptureFlag.Shared.Model
{
    public record GameState(
        IEnumerable<Player> Players,
        IEnumerable<Flag> Flags,
        GameBoard Board,
        bool IsRunning)
    {
        public override string ToString()
        {
            return string.Concat(
                "{\n",
                    "Players: [\n",
                        string.Join(",\n", Players.Select(p => p.ToString())),
                    "]\n",
                    "Flags: [\n",
                        string.Join(",\n", Flags.Select(p => p.ToString())),
                    "]\n",
                    "Board: ", Board.ToString(),
                    "\nIsRunning:", IsRunning.ToString(),
                "\n}");
        }
    }
}