using System;

namespace BlazorCaptureFlag.Shared.Model
{
    public class Flag
    {
        public string Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        public Flag()
        {
            
        }
    }
}