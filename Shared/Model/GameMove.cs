namespace BlazorCaptureFlag.Shared.Model
{
    public enum GameMove
    {
        Up, Right, Down, Left
    }
}