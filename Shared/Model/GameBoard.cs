using System.Collections.Generic;
using System.Text;

namespace BlazorCaptureFlag.Shared.Model
{
    public record GameBoard(int Width, int Height);
}