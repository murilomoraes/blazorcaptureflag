window.game = {
    gamePage: undefined,
    canvasElement: undefined,
    setGamePageRef: function (gamePageRef) {
        this.gamePage = gamePageRef;
    },
    clearGamePageRef: function () {
        this.gamePage = undefined;
    },
    keyboardCommands: {
        ArrowUp: (gamePage) => gamePage.invokeMethodAsync('GoUp'),
        ArrowDown: (gamePage) => gamePage.invokeMethodAsync('GoDown'),
        ArrowLeft: (gamePage) => gamePage.invokeMethodAsync('GoLeft'),
        ArrowRight: (gamePage) => gamePage.invokeMethodAsync('GoRight')
    },
    keyboardListener: function (e) {
        if (window.game.gamePage) {
            let moveFunction = window.game.keyboardCommands[e.key];
            if (moveFunction) {
                moveFunction(window.game.gamePage);
            }
        }
    },
    moveInterval: undefined,
    moveFunction: undefined,
    finishMoveListener: function (e) {
        window.game.moveFunction = undefined;
    },
    touchStartListener: function (e) {
        if (window.game.canvasElement) {
            let c = window.game.canvasElement;

            const touchUpLimit = c.offsetHeight * 0.33;
            const touchDownLimit = c.offsetHeight * 0.67;
            const touchLeftLimit = c.offsetWidth * 0.5;

            const touchX = e.targetTouches[0].clientX;
            const touchY = e.targetTouches[0].clientY;

            console.log("Touch Position: ", touchX, touchY);

            let command = "";
            
            if (touchY - c.offsetTop <= touchUpLimit) command = "ArrowUp";
            else if (touchY - c.offsetTop >= touchDownLimit) command = "ArrowDown";
            else if (touchX - c.offsetLeft <= touchLeftLimit) command = "ArrowLeft";
            else command = "ArrowRight";

            window.game.moveFunction = window.game.keyboardCommands[command];

            e.preventDefault();
        }
    },
    addKeyboardListener: function () {
        document.addEventListener('keydown', this.keyboardListener);
        let c = document.querySelector('#game-board canvas');
        if (c)
        {
            window.game.canvasElement = c;
            window.game.canvasElement.addEventListener('touchstart', this.touchStartListener);
            window.game.canvasElement.addEventListener('touchend', this.finishMoveListener);
            window.game.moveInterval = setInterval(function ()
            {
                if (window.game.moveFunction) {
                    window.game.moveFunction(window.game.gamePage);
                }
            }, 125);
        };
        
    },
    removeKeyboardListener: function() {
        document.removeEventListener('keydown', this.keyboardListener);
        if (window.game.canvasElement)
        {
            window.game.canvasElement.removeEventListener('touchstart', this.mouseDownListener);
            window.game.canvasElement.removeEventListener('touchend', this.finishMoveListener);
            clearInterval(window.game.moveInterval);
        }
    }
}