using System;
using System.Threading.Tasks;
using BlazorCaptureFlag.Shared.Model;

namespace BlazorCaptureFlag.Client.Services
{
    public interface IGameClientService
    {
        GameState State { get; set; }
        string CurrentPlayerId { get; set; }
        int CurrentPlayerPoints { get; }

        event Action<GameState> GameStateChanged;

        void AddFlag(Flag flag);
        void AddPlayer(Player player);
        void MovePlayer(Player player);
        void RemoveFlag(string flagId);
        void RemovePlayer(string playerId);
        void Start(GameState state);
        void Stop();
    }
}