using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorCaptureFlag.Shared.Model;
using BlazorCaptureFlag.Shared.Rules;

namespace BlazorCaptureFlag.Client.Services
{
    public class GameClientService : IGameClientService
    {
        private IList<Flag> _flags;
        private IList<Player> _players;
        private GameBoard _board;

        public bool IsRunning { get; private set; }

        public string CurrentPlayerId { get; set; }
        public int CurrentPlayerPoints => _players
            .FirstOrDefault(p => p.Id == CurrentPlayerId)
            ?.Points ?? 0;

        public GameState State
        {
            get => new GameState(
                _players, _flags, _board, IsRunning);
            set
            {
                _flags = value.Flags.ToList();
                _players = value.Players.ToList();
                _board = value.Board;
                IsRunning = value.IsRunning;
            }
        }

        public event Action<GameState> GameStateChanged;

        public GameClientService() : this(new GameState(
            new List<Player>(), new List<Flag>(), new GameBoard(10, 10), false
        ))
        {
            
        }

        public GameClientService(GameState state)
        {
            State = state;
        }

        public virtual void AddFlag(Flag flag)
        {
            _flags.Add(flag);
            OnGameStateChanged();
        }

        public void AddPlayer(Player player)
        {
            _players.Add(player);
            OnGameStateChanged();
        }

        public void MovePlayer(Player player)
        {
            var movedPlayer = _players.FirstOrDefault(p => p.Id == player.Id);
            if (movedPlayer != null)
            {
                movedPlayer.X = player.X;
                movedPlayer.Y = player.Y;
                movedPlayer.Points = player.Points;
                OnGameStateChanged();
            }
        }

        public void RemoveFlag(string flagId)
        {
            _flags = _flags
                .Where(flag => flag.Id != flagId)
                .ToList();

            OnGameStateChanged();
        }

        public virtual void RemovePlayer(string playerId)
        {
            _players = _players
                .Where(player => player.Id != playerId)
                .ToList();

            OnGameStateChanged();
        }

        public void Start(GameState state)
        {
            IsRunning = true;

            _players = state.Players.ToList();
            _flags = state.Flags.ToList();
            _board = state.Board;
            IsRunning = state.IsRunning;

            OnGameStateChanged();
        }

        public void Stop()
        {
            IsRunning = false;
            OnGameStateChanged();
        }

        private void OnGameStateChanged()
        {
            GameStateChanged?.Invoke(State);
        }
    }
}