using System;
using System.Threading.Tasks;
using BlazorCaptureFlag.Shared.Model;

namespace BlazorCaptureFlag.Server.Services
{
    public interface IGameBackendService
    {
        bool IsRunning { get; }
        GameState State { get; }

        event Func<string, Task> FlagRemoved;

        event Func<GameState, Task> GameStarted;
        event Func<Task> GameStopped;

        event Func<Player, GameState, Task> PlayerAdded;
        event Func<Player, Task> PlayerMoved;
        event Func<string, Task> PlayerRemoved;

        Flag AddFlag();
        Task AddPlayer(string playerId);
        Task MovePlayer(string playerId, GameMove move);
        Task RemoveFlag(string flagId);
        Task RemovePlayer(string playerId);
        Task Start();
        Task Stop();
    }
}