using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;

using BlazorCaptureFlag.Server.Hubs;

namespace BlazorCaptureFlag.Server.Services
{
    public class GameAddFlagService : BackgroundService
    {
        private readonly IHubContext<GameHub, IGameClient> _gameHubContext;
        private readonly IGameBackendService _game;

        public GameAddFlagService(IHubContext<GameHub, IGameClient> gameHubContext, IGameBackendService game)
        {
            _gameHubContext = gameHubContext;
            _game = game;
        }
        
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(2500);
                if (_game.IsRunning)
                {
                    var newFlag = _game.AddFlag();
                    await _gameHubContext.Clients.All.AddFlag(newFlag);
                }
            }
        }
    }
}