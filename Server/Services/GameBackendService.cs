using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorCaptureFlag.Shared.Model;
using BlazorCaptureFlag.Shared.Rules;

namespace BlazorCaptureFlag.Server.Services
{
    public class GameBackendService : IGameBackendService
    {
        private IList<Flag> _flags;
        private IList<Player> _players;
        private readonly GameBoard _board;

        public bool IsRunning { get; private set; }
        public GameState State => new GameState(
            _players, _flags, _board, IsRunning);

        public event Func<string, Task> FlagRemoved;

        public event Func<GameState, Task> GameStarted;
        public event Func<Task> GameStopped;

        public event Func<Player, GameState, Task> PlayerAdded;
        public event Func<Player, Task> PlayerMoved;
        public event Func<string, Task> PlayerRemoved;

        public GameBackendService()
        {
            _flags = new List<Flag>();
            _players = new List<Player>();
            _board = new GameBoard(10, 10);
            IsRunning = false;
        }

        public Flag AddFlag()
        {
            var random = new Random();
            var flag = new Flag
            {
                Id = Guid.NewGuid().ToString(),
                X = random.Next(_board.Width - 1),
                Y = random.Next(_board.Height - 1)
            };
            _flags.Add(flag);
            return flag;
        }

        public async Task AddPlayer(string playerId)
        {
            int playerCount = _players.Count;
            var random = new Random();
            var player = new Player()
            {
                Id = playerId,
                X = random.Next(_board.Width - 1),
                Y = random.Next(_board.Height - 1)
            };
            _players.Add(player);
            await PlayerAdded?.Invoke(player, State);
            if (playerCount == 0)
            {
                await Start();
            }
        }

        public async Task MovePlayer(string playerId, GameMove move)
        {
            var player = _players.FirstOrDefault(
                p => p.Id == playerId);
            
            if (player != null &&
                GameRules.MovePlayer(player, _board, move))
            {
                await CheckCollisions(player);
                await PlayerMoved?.Invoke(player);
            }
        }

        public Task RemoveFlag(string flagId)
        {
            _flags = _flags
                .Where(flag => flag.Id != flagId)
                .ToList();

            return FlagRemoved?.Invoke(flagId) ?? Task.CompletedTask;
        }

        public async Task RemovePlayer(string playerId)
        {
            _players = _players
                .Where(player => player.Id != playerId)
                .ToList();

            await PlayerRemoved?.Invoke(playerId);
            if (_players.Count == 0)
                await Stop();
        }

        public Task Start()
        {
            IsRunning = true;

            foreach (var player in _players)
            {
                player.ResetPoints();
            }

            _flags = new List<Flag>();

            return GameStarted?.Invoke(State);
        }

        public Task Stop()
        {
            IsRunning = false;
            return GameStopped?.Invoke() ?? Task.CompletedTask;
        }

        private async Task CheckCollisions(Player player)
        {
            foreach (var flag in _flags)
            {
                if (player.X == flag.X && player.Y == flag.Y)
                {
                    player.IncrementPoints();
                    await RemoveFlag(flag.Id);
                    break;
                }
            }
        }
        
    }
}