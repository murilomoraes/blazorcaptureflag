using Microsoft.AspNetCore.SignalR;
using BlazorCaptureFlag.Server.Services;
using BlazorCaptureFlag.Shared.Model;
using System.Threading.Tasks;
using System;

namespace BlazorCaptureFlag.Server.Hubs
{
    public class GameHub : Hub<IGameClient>
    {
        private readonly IGameBackendService _gameService;

        public GameHub(IGameBackendService gameService)
        {
            _gameService = gameService;
            _gameService.FlagRemoved += OnFlagRemoved;
            _gameService.GameStarted += OnGameStarted;
            _gameService.GameStopped += OnGameStopped;
            _gameService.PlayerAdded += OnPlayerAdded;
            _gameService.PlayerMoved += OnPlayerMoved;
            _gameService.PlayerRemoved += OnPlayerRemoved;
        }

        public Task OnFlagRemoved(string flagId)
        {
            return Clients.All.RemoveFlag(flagId);
        }

        public Task OnGameStarted(GameState state)
        {
            return Clients.All.GameStart(state);
        }

        public Task OnGameStopped()
        {
            return Clients.All.GameStop();
        }

        public Task OnPlayerAdded(Player player, GameState state)
        {
            var taskconn = Clients.Caller.PlayerConnected(state);
            var taskadd = Clients.Others.AddPlayer(player);
            return Task.WhenAll(taskadd, taskconn);
        }

        public Task OnPlayerMoved(Player player)
        {
            return Clients.All.PlayerMoved(player);
        }

        public Task OnPlayerRemoved(string playerId)
        {
            return Clients.Others.RemovePlayer(playerId);
        }

        public Task AddPlayer()
        {
            return _gameService.AddPlayer(Context.ConnectionId);
        }

        public Task MovePlayer(string playerId, GameMove move)
        {
            return _gameService.MovePlayer(playerId, move);
        }

        public override Task OnDisconnectedAsync(Exception e)
        {
            return _gameService.RemovePlayer(Context.ConnectionId);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _gameService.FlagRemoved -= OnFlagRemoved;
                _gameService.GameStarted -= OnGameStarted;
                _gameService.GameStopped -= OnGameStopped;
                _gameService.PlayerAdded -= OnPlayerAdded;
                _gameService.PlayerMoved -= OnPlayerMoved;
                _gameService.PlayerRemoved -= OnPlayerRemoved;
            }
            base.Dispose(disposing);
        }
    }
}