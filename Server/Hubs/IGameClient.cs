using System.Threading.Tasks;
using BlazorCaptureFlag.Shared.Model;

namespace BlazorCaptureFlag.Server.Hubs
{
    public interface IGameClient
    {
        Task AddFlag(Flag flag);
        Task AddPlayer(Player player);
        Task GameStart(GameState state);
        Task GameStop();
        Task PlayerConnected(GameState state);
        Task PlayerMoved(Player player);
        Task RemoveFlag(string flagId);
        Task RemovePlayer(string flagId);
    }
}